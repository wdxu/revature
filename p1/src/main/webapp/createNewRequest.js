async function createNewRequest()
{
	document.getElementById('amountError').innerHTML = "";
    let a = document.getElementById("amount").value;
	//let p = document.getElementById("receipt").value;
	if(a == null || a == "")
	{
		document.getElementById('amountError').innerHTML = "<p style=\"color:red\"> Please submit an actual amount <p>";
		return null;
	}
	a = parseFloat(a.toString().replace(/\$|\,/g,''));
    let r = document.getElementById("reason").value;
    let t = null;
	if(document.getElementById("btnradio1").checked == true)
	{
		t = 0;
	}
	if(document.getElementById("btnradio2").checked == true)
	{
		t = 1;
	}
	if(document.getElementById("btnradio3").checked == true)
	{
		t = 2;
	}
	if(document.getElementById("btnradio4").checked == true)
	{
		t = 3;
	}
	if(t == null)
	{
		document.getElementById('typeError').innerHTML = "<p style=\"color:red\"> Please select a type <p>";
		return null;
	}
	console.log(a);
	console.log(r);
	console.log(t);
    let request = {
        requestID : null, //don't care
        username : null,
        amount : a,
        reason : r,
        type : t,
        date : null, //don't care
        status : null, //don't care
        usernameResolve : null, //doesn't exist yet
        dateResolve : null //doesn't exist yet
    };//receipt : p
    let response = await fetch("/p1/CreateRequestServlet", 
								{method:"POST", 
								headers: {"Content-Type": "application/json", "Accept": "application/json"}, 
								body: JSON.stringify(request)});
    let result = await response.text();
    console.log(result);
	if(result == "SM")
	{
		alert("Request Submitted");
		window.location.replace("manager.html");
	}
	if(result == "SE")
	{
		alert("Request Submitted");
		window.location.replace("employee.html");
	}
	if(result == "NV")
	{
		alert("Session Not Valid");
		logoff();
		window.location.replace("login.html");
	}
    return result;
}

function formatCurrency(num) 
{
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
		num = "0";
	let sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	let cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (let i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + '$' + num + '.' + cents);
}

async function sessionValidation()
{
    let response = await fetch("/p1/SessionValidationServlet", 
								{method:"POST"});
    let result = await response.text();
    console.log(result);
	if(result == "fail")
	{
		window.location.replace("login.html");
	}
	if(result == "succE")
	{
		if(window.location.href != "http://localhost:8080/p1/newRequestEmployee.html")
		{
			window.location.replace("http://localhost:8080/p1/newRequestEmployee.html");
		}
	}
	if(result == "succM")
	{
		if(window.location.href != "http://localhost:8080/p1/newRequestManager.html")
		{
			window.location.replace("http://localhost:8080/p1/newRequestManager.html");
		}
	}
}

async function logout()
{
    let response = await fetch("/p1/LogoutServlet", 
								{method:"POST"});
    let result = await response.text();
    console.log(result);
	if(result =="LoggedOut")
	{
		window.location.replace("login.html");
	}
    else
    {
        console.log("log off failed???");
    }
    return result;
}
window.onLoad = sessionValidation();