async function getEmployees()
{
	document.getElementById('replacable').innerHTML = "";
    let response = await fetch("/p1/GetEmployeesServlet", 
								{method:"POST"});
	let al = await response.json();
    //let result = await response.text();
    //var al = JSON.parse(result);
	console.log(al);
	let s = "";
	s +=`
	<table class="table table-dark">
	<thead>
	  <tr>
	    <th scope="col">ID</th>
	    <th scope="col">Username</th>
	    <th scope="col">Email</th>
	    <th scope="col">Firstname</th>
	    <th scope="col">Lastname</th>
	    <th scope="col">Role</th>
	  </tr>
	</thead>
	<tbody>`;
	for(let i = 0; i<al.length+1; i++)
	{
		if(i<al.length)
		{
			let m;
			if(al[i].isManager == true)
			{
				m = "Manager";
			}
			else
			{
				m = "Employee";
			}
			//If I really want to dick around, make a "fire" button
			//<td> <button class="btn-primary" type="button" onclick="changeStatus(`+al[i].requestID+`, 1)">Approve</button> </td>
			s +=`
			<tr>
				<td> ` + al[i].id + ` </td>
				<td> ` + al[i].username + ` </td>
				<td> ` + al[i].email + ` </td>
				<td> ` + al[i].firstname + ` </td>
				<td> ` + al[i].lastname + ` </td>
				<td> ` + m + ` </td>
			</tr>`;
		}
		else
		{
			s += `</tbody>`;
			s += `</table>`;
		}
	}
	document.getElementById('replacable').innerHTML += s;
//    console.log(result);
//	if(result=="SM")
//	{
//		window.location.replace("manager.html");
//	}
//	if(result=="SE")
//	{
//		window.location.replace("employee.html");
//	}
	console.log(document.getElementById('replacable').innerHTML);
}

async function sessionValidation()
{
    let response = await fetch("/p1/SessionValidationServlet", 
								{method:"POST"});
    let result = await response.text();
    console.log(result);
	if(result == "fail")
	{
		window.location.replace("login.html");
	}
	if(result == "succE")
	{
		//should never happen
		if(window.location.href != "http://localhost:8080/p1/employee.html")
		{
			window.location.replace("http://localhost:8080/p1/employee.html");
		}
	}
	if(result == "succM")
	{
		if(window.location.href != "http://localhost:8080/p1/viewEmployees.html")
		{
			window.location.replace("http://localhost:8080/p1/viewEmployees.html");
		}
	}
}

async function logout()
{
    let response = await fetch("/p1/LogoutServlet", 
								{method:"POST"});
    let result = await response.text();
    console.log(result);
	if(result =="LoggedOut")
	{
		window.location.replace("login.html");
	}
    else
    {
        console.log("log off failed???");
    }
    return result;
}

//function bundle()
//{
//	sessionValidation();
//	getEmployees();
//}


window.addEventListener("load",function() {
    sessionValidation();
},false);

window.addEventListener("load",function(){
	getEmployees();
},false);